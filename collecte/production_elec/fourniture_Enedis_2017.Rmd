---
title: Traitement des données fournies par Enedis via l'espace _Collectivités_ pour la v1
author: "Juliette Engelaere-Lefebvre"
date: "`r format(Sys.Date(), '%d %B %Y')`"
output:
  html_document:
    df_print: paged
    toc: yes
    toc_depth: '2'
---
## Chargement des packages

```{r packages Enedis, eval=TRUE, echo=TRUE, warning=FALSE, message=FALSE}

library(dplyr)
library(sf)
library(tidyr)
library(stringr)
library(forcats)
library(xlsx)
library(COGiter)
library(ggplot2)
source("collecte/Ajout_communes_insulaires_aux_EPCI.R", encoding = "UTF-8")

```

## Paramètres du script
  
```{r parametres Enedis, eval=TRUE, echo=TRUE}

mil <- 2017
reg <- "52"

dep_reg <- filter(departements, REG==reg) %>%
  select(DEP) %>%  mutate(DEP=as.character(DEP)) %>%
  pull(DEP)

```



# Lecture des données de production Enedis
Quelques modifs manuelles du fichier transmis par Emmanuel Heurtebise avant import R :  
- une formule en en-tete pour dégrouper le nom des champs (ajout de l'annee),  
- remplissage des champs vide avec 0, pour que les nuls ne se confondent pas avec des secrets,  
- enregistrement de la feuille _Prod totale_ au format csv (encodage _utf8_, séparateur de champ _;_, 
option _citer toutes les cellules de textes_ sélectionnée et option _Enregistrer comme affiché_ décochée).

```{r lecture des données Enedis, eval=TRUE, echo=TRUE, warning=FALSE, message=TRUE}

path_enedis <- "extdata/donnees_client/demande_enedis/"

prod_enedis <- read.csv2(file = paste0(path_enedis, "prod_enedis_2011-2017.csv"), header = TRUE, sep = ";",
                         dec = ",", na.strings = "S", encoding = "UTF-8")

lib_filiere <- tibble(Filiere.de.production=unique(prod_enedis$Filiere.de.production) %>% sort(), 
                      filiere=c("autre", "bioenergies", "coge", "eolien", "hydro", "pv")) %>%
  mutate(across(everything(), as.factor))
```

Le fichier contient des informations sur la filière cogénération et sur la tension de raccordement. Ces informations sont abandonnées dans la suite des traitements.

# Mise à jour des référentiels géographiques
Le fichier mélange des cartes IRIS, communales et EPCI de différentes époques.
Les données de production d'une année N sont fournie en géographie 1er janv de l'année N...
Les données EPCI et département sont inexploitables en évolution. Il faut les recomposer à partir des mailles inférieures.

## Test des communes

```{r ajout champ commune à jour}

prod_enedis_com <- left_join(prod_enedis, lib_filiere) %>%
  filter(Maille == "COMMUNE", filiere != "coge") %>%
  pivot_longer(names_to = "indicateur", values_to = "valeur", Sites2011:Energie.totale.en.kWh2017) %>%
  mutate(valeur_NA_nul = if_else(is.na(valeur), 0, valeur),
         indic = as.factor(paste0(filiere, "_", indicateur))) %>%
  select(-Filiere.de.production, -Maille, -Territoire, -Segment, -indicateur, -filiere) %>%
  left_join(table_passage_com_historique, by=c("Code.INSEE" = "DEPCOM_HIST")) %>%
  select(-Code.INSEE) %>%
  group_by(DEPCOM, indic) %>%
  summarise(across(c(valeur, valeur_NA_nul), sum), .groups = "drop") %>%
  left_join(communes_info_supra, by="DEPCOM") %>%
  as_tibble() %>%
  mutate_if(is.character, as.factor)

```

Environ 1/4 des données communales sont entachées de secret (`r nrow(filter(prod_enedis_com,  is.na(valeur)))` données sur `r nrow(prod_enedis_com)` ne sont pas ok).

> Proposition de répartir les valeur secrétisées, agrégées au département, au prorata du nb d'installations
> Proposition d'utiliser les données de laplateforme opendata si le nb d'instal est connu pour récupérer les communes des epci limitrophes, vu que la géographie n'est pas mieux gérée et de le caviardage n'est pas restreint

# Quid de la filière autre ?
`r nrow(filter(prod_enedis_com, indic=="autre_Sites2017"))` installations de la région relèvent de la filière autre. Elles sont situées dans les communes :
_`r filter(prod_enedis_com, indic=="autre_Sites2017") %>% select(DEP, NOM_DEPCOM) %>% mutate(lib=paste0(NOM_DEPCOM, " (", DEP, ")")) %>% select(-DEP, -NOM_DEPCOM) %>% paste()`_
> Faut-il les garder ?

nota, puissances respectives : 810 kW, 7759 kW, 750 kW

# Répartition des valeur secrétisées au prorata du nb d'installations

On réunit par type d'installation, par département et par année, le nb d'installations et leurs caractéristiques de puissance et de production selon que les données soient caviardées ou non.

```{r amorce de la table à redresser}

a_redresser <- mutate(prod_enedis, dep_old = as.factor(substr(Code.INSEE, 1,2)), # attention les champs dep_old et com_old
                      com_old = as.factor(substr(Code.INSEE, 1,5)) )%>% # ne fonctionnent pas à toutes les échelles
  select(dep_old, com_old, starts_with("Sites"), everything()) %>% # on isole les nb de sites par annee au début
  pivot_longer(names_to = "indicateur", values_to = "valeur", Puissance.totale.en.kW2011:Energie.totale.en.kWh2017, 
               names_transform = list(indicateur = as.factor)) %>% # passage au format long sur les seuls indicateurs de puissance et de production
  mutate(indic_cav = case_when(Maille=="REGION" ~ "tot_reg", # différenciation des enregistrmt selon statut
                             Maille=="DEPARTEMENT" ~ "tot_dep",
                             is.na(valeur) ~ "caviardees",
                             T ~ "pas caviardees") %>% as.factor,
         annee = str_extract(string = indicateur, pattern = "\\d\\d\\d\\d") %>% as.factor, # passage de l'annee en dimension
         indicateur = gsub("\\d", "", indicateur) %>% as.factor) %>% # purgeage annee des lib indicateurs
  pivot_longer(names_to = "annee_sites", values_to = "nb_sites", starts_with("Sites")) %>% # astuce pour ajouter à coté de la valeur d'indicateur le nb de sites correspondant
  filter(annee_sites == paste0("Sites", annee)) %>%
  select(-annee_sites, -Territoire)

```

On établit ensuite des coefficients unitaires de puissance et de production en soustrayant du total départemental (ou régional si département caviardé) les données non caviardées à la maille iris puis on divise l'ensemble par le nombre de sites caviardés à la maille iris.

Le calcul de ces coefficients unitaires se fait à l'IRIS car c'est l'échelon infra départemental le moins caviardé.
Toutefois, certaines totaux communaux sont disponibles si plusieurs iris sont caviardés, pour ne pas perdre la valeur de ces totaux communaux une étape préalable de repartition des totaux communaux connus aux iris qui le composent est nécessaires

## répartition des totaux com connus aux iris caviardés qui le composent

On calcule d'abord une matrice de coefficients unitaires communaux de redressement
```{r constitution de la table des coeff communaux de répartition }
# sélection des enregistrements concernés
com_non_cav <- filter(a_redresser, Maille == "COMMUNE", !is.na(valeur), valeur > 0) %>%
  select(com_old, Filiere.de.production, Segment, annee) %>%
  unique()

iris_cav <- filter(a_redresser, Maille == "IRIS", is.na(valeur)) %>%
  select(com_old, Filiere.de.production, Segment, annee) %>%
  unique()

# construction de la matrice communale des coefficients unitaires de redressement
a_repartir_com_dans_iris = inner_join(com_non_cav, iris_cav) %>%
  inner_join(a_redresser) %>%
  mutate(indic_cav = as.factor(if_else(Maille=="COMMUNE", "tot_com", as.character(indic_cav)))) %>%
  select(-Code.INSEE, -Maille) %>%
  group_by(across(where(is.factor)|where(is.character))) %>%
  summarise(across(everything(), sum), .groups = "drop") %>%
  group_by(com_old, annee, dep_old, indicateur, Filiere.de.production, Segment) %>%
  arrange(indic_cav) %>% # on met dans l'ordre pour que les lead et lag fonctionnent
  mutate(valeur = case_when(indic_cav != "caviardees" ~ valeur, # cas des iris non caviardés et des totaux com
                      # cas où tout est caviardé à l'iris mais pas le tot com :
                            lead(indic_cav)=="tot_com" & !is.na(lead(valeur)) ~ lead(valeur),
                      # cas le plus fréquent où il existe des données iris non caviardées
                            lead(indic_cav)=="pas caviardees" ~ lead(valeur, 2) - lead(valeur)
                           ),
         nb_sites_verif = case_when(
           indic_cav=="caviardees" & lead(indic_cav) == "pas caviardees" ~ lead(nb_sites,2)-nb_sites-lead(nb_sites),
           indic_cav=="caviardees" & lead(indic_cav)=="tot_com" ~ lead(nb_sites)-nb_sites)
         ) %>%
  mutate(valeur_unitaire = valeur/nb_sites) %>%
  filter(indic_cav=="caviardees", nb_sites_verif==0) %>%
  select(-indic_cav, -valeur, -nb_sites, -nb_sites_verif)


```

On estime ensuite les productions et puissances raccordées des IRIS dont les totaux communaux sont connus.

```{r table des iris redressés grace aux totaux communaux}

# on redresse les enregistrements IRIS concernés
redresse_iris_com <- inner_join(com_non_cav, iris_cav) %>%
  inner_join(a_redresser) %>%
  filter(Maille == "IRIS", is.na(valeur)) %>% # sélection des enregistrements concernés
  left_join(a_repartir_com_dans_iris) %>% # jointure à  la matrice de redressement
  mutate(valeur_redressee = if_else(is.na(valeur), nb_sites * valeur_unitaire, valeur), # calcul des estimations
         estimation_com=if_else(is.na(valeur_redressee), NA, is.na(valeur))) %>% # création d'un champ qui indique la provenance communale de la valeur redressee
  select(-valeur_unitaire, -indic_cav)

# puis jointure à la table globale pour ajouter les valeurs IRIS estimées
a_redresser_0 <- left_join(a_redresser, redresse_iris_com) %>%
  mutate(indic_cav = as.factor(if_else(!is.na(estimation_com), "pas caviardees", as.character(indic_cav))),
         valeur = if_else(!is.na(estimation_com), valeur_redressee, valeur)) %>%
  select(-valeur_redressee)

rm(redresse_iris_com)
```


## Répartition des totaux dep ou reg aux iris caviardés qui les composent

On procède de la même manière mais avec les totaux de l'échelle départementale ou régionale pour redresser les IRIS dont les totaux communaux sont également caviardés. Pour cela on repart de la table globale amorcée plus haut, avec entre autres les IRIS redressés grâce aux totaux communaux.

```{r constitution de la matrice départementale des coefficients unitaires de redressement }

# on agrège les iris à l'échelle départementale selon qu'ils soient caviardés ou non pour constituer la matrice de redressement

a_repartir_1 <- filter(a_redresser_0, Maille %in% c("DEPARTEMENT", "IRIS", "REGION")) %>%
  select(-Code.INSEE, -com_old, -Maille) %>%
  group_by(across(where(is.factor)|where(is.character))) %>%
  summarise(nb_sites = sum(nb_sites), valeur = sum(valeur), .groups = "drop") %>%
  group_by(annee, dep_old, indicateur, Filiere.de.production, Segment) %>% 
  arrange(indic_cav) %>% # on met dans l'ordre pour que les lead et lag fonctionnent
  filter(nb_sites != 0) # on enlève les lignes qui ne correspondent à aucun site

# on duplique les ratios régionaux au cas où les données départementales sont aussi caviardées
coeff_reg <- filter(a_repartir_1, dep_old == reg, Segment== "BTINF36") %>%
  rename(reg = dep_old) %>%
  full_join(data.frame(reg = reg, dep_old = dep_reg), .) %>%
  select(-reg)

# établissement de la matrice départementale des coefficients unitaires de redressement (suite et fin)
a_repartir_2 <- filter(a_repartir_1, dep_old != "52") %>%
  bind_rows(coeff_reg) %>%
  mutate(valeur = case_when(indic_cav != "caviardees" ~ valeur, # cas des agregats IRIS non caviardés ou dep ou reg
                      # cas où tout est caviardé à l'iris mais pas le tot dep :
                            lead(indic_cav) == "tot_dep" & !is.na(lead(valeur)) ~ lead(valeur),
                      # cas où tout est caviardé à l'iris et au dep, on va chercher la valeur reg, il faudra ajuster le nb de sites :
                            lead(indic_cav) == "tot_dep" & is.na(lead(valeur)) ~ lead(valeur, 2),
                      # cas le plus fréquent où il existe des données iris non caviardées
                            lead(indic_cav) == "pas caviardees" ~ lead(valeur, 2)-lead(valeur)
                            ),
         nb_sites=if_else(lead(indic_cav)=="tot_dep" & is.na(lead(valeur)), lead(nb_sites,2), nb_sites)) %>%
  mutate(valeur_unitaire = valeur/nb_sites) %>%
  filter(indic_cav=="caviardees") %>%
  select(-indic_cav, -valeur, -nb_sites)
```

Cette matrice de passage 'a_repartir_2' permet ensuite d'estimer les données à l'iris et départementales caviardées à partir du nombre de sites.

```{r redressement des données iris et dep}
redresse_iris_dep_reg <- filter(a_redresser_0, Maille %in% c("DEPARTEMENT", "IRIS", "REGION")) %>%
  left_join(a_repartir_2) %>%
  mutate(valeur_redressee = if_else(is.na(valeur), nb_sites * valeur_unitaire, valeur),
         estimation = case_when(estimation_com  ~ TRUE,
                                valeur_redressee==valeur | is.na(valeur_redressee) ~ FALSE,
                                TRUE ~ TRUE),
         type_estimation = case_when(estimation_com  ~ "reventilation du total communal",
                                     valeur_redressee == valeur | is.na(valeur_redressee) ~ "aucune, donnee brute",
                                     TRUE ~ "reventilation du total départemental ou régional")) %>%
  mutate_if(is.character, as.factor) %>%
  select(TypeZone=Maille, CodeZone = Code.INSEE, everything(), -valeur, -valeur_unitaire, -dep_old, -indic_cav, -estimation_com)
```


Les données communales sont ensuite obtenues par agrégation des données à l'iris.
Les  5 premiers caractères des iris de l'année 2016 correspondent à des codes communes de l'année 2015, la jointure doit se faire sur les codes communes à jour

```{r redressement des données communales}
aggr_com_redresse_iris <- filter(redresse_iris_dep_reg, TypeZone=="IRIS") %>%
  mutate(TypeZone = "COMMUNE", com_old = as.factor(substr(CodeZone, 1,5))) %>%
  select(-CodeZone, -type_estimation) %>%
  left_join(table_passage_com_historique, by=c("com_old"="DEPCOM_HIST")) %>%
  select(-com_old) %>%
  group_by(across(where(is.factor)|where(is.character))) %>% 
  summarise(nb_sites_iris = sum(nb_sites), valeur_redressee = sum(valeur_redressee), .groups = "drop_last") 


redresse_com <- filter(a_redresser_0, Maille == "COMMUNE") %>%
  left_join(table_passage_com_historique, by=c("Code.INSEE" = "DEPCOM_HIST")) %>%
  select(-Code.INSEE, -indic_cav, -dep_old, -com_old, -Maille) %>%
  group_by(across(where(is.factor)|where(is.character))) %>%  
  summarise(across(everything(), sum), .groups = "drop_last") %>%
  full_join(aggr_com_redresse_iris) %>%
  mutate(estimation = case_when(valeur_redressee==valeur | is.na(valeur_redressee) ~ FALSE,
                                TRUE ~ TRUE),
         type_estimation = case_when(
           valeur_redressee == valeur | is.na(valeur_redressee) ~ "aucune, donnee brute",
           TRUE ~ "reventilation du total départemental ou régional")) %>%
  select(TypeZone, CodeZone = DEPCOM, everything(), -valeur, -estimation_com, -nb_sites_iris)

rm(aggr_com_redresse_iris)

```

# Compilation des données communales et supra
Le but de cette partie datamartage est d'assembler les données com, dep, reg.
Les données à l'iris seront intégrées ultérieurement car un redressement géographique est à opérer.
On vérifie d'abord que toutes les communes de la régions sont couvertes par la source Enedis. (ie qu'elle contiennent au moins une installation une année donnée)

```{r liste des communes manquantes}

 com_sans_inst <- filter(communes, REG==reg) %>% 
  select(CodeZone=DEPCOM) %>%
  anti_join(redresse_com) %>%
  mutate(TypeZone = "COMMUNE", Filiere.de.production="Autres filières", Segment="HTA",
         indicateur= "Energie.totale.en.kWh", annee="2017", nb_sites=0,
         type_estimation="aucune, donnee brute", valeur_redressee=0, estimation=FALSE)


liste <- paste0(as.list(as.character(com_sans_inst$CodeZone)), collapse = ", ")
paste0(nrow(com_sans_inst), " communes sans installations : ", " (", unlist(liste), ")")

```

Pour ne pas confondre les valeurs secrétisées avec les valeurs nulles, on ajoute les communes sans installations avec des lignes à zéro.

```{r Ajouts des communes manquantes aux différentes dimensions}
compil_com_dep_reg <- filter(redresse_iris_dep_reg, TypeZone != "IRIS") %>%
  select(-com_old) %>%
  bind_rows(redresse_com) %>%
  bind_rows(com_sans_inst) %>%
  mutate(across(where(is.factor), as.character)) %>%
  mutate(across(where(is.character), as.factor)) %>% # pour ne garder que les levels utilisés des facteurs
  unique() %>% # pour dédoublonner les lignes dédiées au nombre de sites
  complete(nesting(TypeZone, CodeZone), annee, Filiere.de.production, Segment, indicateur, fill =
             list(nb_sites=0, type_estimation="aucune, donnee brute", valeur_redressee=0, estimation=FALSE))

nb_lignes_attendues <- (nrow(filter(communes, REG == reg))+6)*length(levels(redresse_com$annee))*6*2*3


```

Vérification qu'on obtient bien le nombre de lignes attendu : `r nrow(compil_com_dep_reg) == nb_lignes_attendues`

```{r passage de la variable nb_sites au format long}

# on isole l'indicateur dans un df à part
compil_com_dep_reg_2 <- select(compil_com_dep_reg, TypeZone, CodeZone, Filiere.de.production, Segment, annee,
                               valeur_redressee = nb_sites, estimation, type_estimation) %>%
  mutate(indicateur = "Nombre de sites", estimation = FALSE, type_estimation = "aucune, donnee brute") %>%
  unique()

# on rassemble les 3 indicateurs dans un unique df

compil_com_dep_reg_3 <- select(compil_com_dep_reg, -nb_sites) %>%
  bind_rows(compil_com_dep_reg_2) %>%
  mutate(across(where(is.character), as.factor))

```

On peut maintenant calculer les totaux EPCI et DEP, on perd pour ces mailles l'info sur l'origine de la valeur (brute ou estimation).

```{r calcul des totaux EPCI et départements à partir des données communales}

compil_epci_dep_0 <- filter(compil_com_dep_reg_3, TypeZone=="COMMUNE") %>%
  select(-estimation, -type_estimation) %>%
  left_join(communes_info_supra %>% select(DEPCOM, EPCI, DEP), 
            by=c("CodeZone" = "DEPCOM"))

# on n'utilise pas cogifier pour garder l'ile d'yeu dans les EPCI
compil_epci_0 <- group_by(compil_epci_dep_0, CodeZone = EPCI, annee, Filiere.de.production, Segment, indicateur) %>%
  summarise(valeur_redressee = sum(valeur_redressee), .groups = "drop") %>%
  mutate(TypeZone = "Epci")
  
compil_dep_0 <- group_by(compil_epci_dep_0, CodeZone = DEP, annee, Filiere.de.production, Segment, indicateur) %>%
  summarise(valeur_redressee = sum(valeur_redressee), .groups = "drop") %>%
  mutate(TypeZone = "Départements")
  
compil_epci_dep <- bind_rows(compil_epci_0, compil_dep_0) %>% 
  mutate(estimation = TRUE, type_estimation = "recalcul à partir des données communales")

rm(compil_epci_0, compil_dep_0, compil_epci_dep_0)

```
a terme raffiner le calcul des totaux dep en conservant les totaux départementaux de compil_com_reg_dep_3 qui ne changent pas de valeur avec le départ de Freigné (attention la question des arrondis fout le bazar )

```{r compilation multi_echelle}
compil_com_epci_dep_reg <- filter(compil_com_dep_reg_3, TypeZone=="COMMUNE" | TypeZone=="REGION") %>%
  bind_rows(compil_epci_dep)

```


## vérifications

Etant donné le nombre importants de jointures dans les traitements précédents, vérifions que le nb de sites par année à l'échelle régionale n'a pas été altéré par rapport au fichier d'origine.

```{r "vérification que le nb de sites par annee n'a pas été alteré"}
verif_nb_sites <- filter(compil_com_epci_dep_reg, indicateur=="Nombre de sites") %>%
  group_by(TypeZone, annee) %>%
  summarise(valeur=sum(valeur_redressee), .groups = "drop")%>%
  pivot_wider(names_from = "annee", values_from = "valeur")
verif_nb_sites

```
On retrouve les données du tableau croisé dynamique réalisé dans le tableur source (sauf pour EPCI, mais pas ile d'Yeu donc ok).



```{r "vérification que les puissances par annee n'ont pas été alterées"}
verif_puissance <- filter(compil_com_epci_dep_reg, indicateur == "Puissance.totale.en.kW") %>%
  group_by(TypeZone, annee) %>%
  summarise(valeur = round(sum(valeur_redressee)/1000, digits = 1), .groups = "drop") %>%
  ungroup() %>%
  pivot_wider(names_from = "annee", values_from = "valeur")

verif_puissance

```

```{r "vérification que toutes les cases sont remplies"}

filter(compil_com_epci_dep_reg, is.na(estimation)|is.na(valeur_redressee))

```

# Datamartage des données communales et supra - calculs des indicateurs
Pour l'application POC EnR TEO, la segmentation par tension de raccordement est inutile, les données en évolution ne concernent que l'échelle communales et supra, et on ne conserve que les données EnR&R (pas cogé ni autre)

```{r datamartage}

compil_com_epci_dep_reg_2017 <- filter(compil_com_epci_dep_reg, Filiere.de.production != "Cogénération",
                                    Filiere.de.production != "Autres filières") %>%
  mutate(across(where(is.character), as.factor),
         across(where(is.factor), fct_drop),
         TypeZone = fct_infreq(TypeZone) %>% fct_recode("Communes" = "COMMUNE", "Régions" = "REGION")) %>%
  group_by(TypeZone, CodeZone, Filiere.de.production, annee, indicateur)%>%
  arrange(Segment) %>%
  select(-Segment) %>%
  summarise(valeur = sum(valeur_redressee), estimation = first(estimation), type_estimation = first(type_estimation), 
            .groups = "drop")
  
```


# Versement des données dommunales redressées au SGBD
compil_com_epci_dep_reg_2017 correspond à l'ensemble de la source (hors données à l'iris, et prod non renouvelable) nettoyée, elle peut être versée dans le SGBD
```{r SGBD}
library(RPostgreSQL)
library(rpostgis)
library(DBI)

nom_sgbd <- function(x) {
    tolower(x) %>%
        gsub(pattern = ".", replacement = "_", x = . , fixed = T)
}

source("collecte/sgbd/con_sgbd_prod.R") # script de connexion au sgbd postgis de la DREAL contenant les identifiants

postgresqlpqExec(con, "SET client_encoding = 'windows-1252'")

dbSchema(con, "electricite")

dbWriteTable(con, c("electricite", paste0("prod_elec_renouv_2011_", mil, "_r", reg)), rename_all(compil_com_epci_dep_reg_2017, nom_sgbd), 
             row.names=FALSE, overwrite=TRUE)

dbGetQuery(con, paste0("COMMENT ON TABLE electricite.\"prod_elec_renouv_2011_", mil, "_r", reg, "\" IS 'La table comprend les productions électriques renouvelables, les puissances raccordées et le nb d installations à toutes les échelles (com, epci, dep, region) depuis 2011 jusque ", mil, ". Il correspond à la mise aux réfrentiels géographiques actuels (", format(Sys.Date(), '%d %B %Y'), ") de données fournies par Enedis dans le cadre du POC. Pour palier à la secrétisation des données à caractère personnel, les données caviardées ont été estimées au pro rata du nb d installations.';"))

spec_prod <- rename_all(compil_com_epci_dep_reg_2017, nom_sgbd) %>%
  summarise_all(class) %>%
  gather(variable, class)

dbWriteTable(con, c("electricite", "prod_spec"), spec_prod, row.names=FALSE, overwrite=TRUE)


dbDisconnect(con)

```



# Redressement géographique des iris

### chargement du référentiel des IRIS
```{r chargement des IRIS, eval=TRUE, echo=TRUE, warning=FALSE, message=FALSE}

path_iris <- "extdata/donnees_internes/"

if ( !file.exists(paste0(path_iris,"iris.Rdata")) ) { source(paste0(path_iris,"/collecte_iris.R")) }
load(paste0(path_iris,"iris.Rdata"))

iris_fr_entiere_geo <- mutate(iris_fr_entiere_geo, 
                              EPCI=if_else(grepl("ZZ", EPCI), paste0("ZZZZ", DEPCOM), as.character(EPCI)) %>% as.factor,
                              NOM_EPCI=if_else(grepl("ZZ", EPCI), as.character(NOM_DEPCOM), as.character(NOM_EPCI)) %>% as.factor )


iris_reg <- filter(iris_fr_entiere_geo, REG == reg ) %>% 
  select(CODE_IRIS:NOM_EPCI)

# création de la table des iris des communes non subdivisées (périmetre IRIS = périmètre commune)
iris_com_reg <- group_by(iris_reg, DEPCOM) %>%
  filter(n_distinct(CODE_IRIS) == 1) %>%
  ungroup() %>%
  as.data.frame()

# table redressement manuel iris en echec (cf script conso)
tab_pass_2 <- data.frame(IRIS_INI=c("240530201", "276260000", "392260000", "393820000", "395090000", "490630102", "490940000", "491540000",
                                    "492790000", "493460000", "501800000", "610270000", "611270000", "612360000", "614410000",
                                    "614700000"),
                        IRIS_FIN=c(NA, "270890000", "392090000","392090000", "392090000", "490630103", "492610000", "492610000", "492610000",
                                   "492610000", "505920000", "611530000", "611530000", "611530000", "611530000", "611530000"),
                        TYP_IRIS=c(NA, rep("Z", 5), "H", rep("Z", 9)))

table_passage_iris_hist <- group_by(table_passage_iris_historique, IRIS_INI) %>%
  filter(n()==1) %>% ungroup() %>%
  bind_rows(tab_pass_2)

rm(path_iris, iris_fr_entiere_geo)

gc()

```

Etant donné les difficultés posées par la multiplicité des référentiels IRIS, on se concentre sur les données du dernier millésime,
qui seules sont susceptibles d'être utiles dans le cadre du POC.

### test appariement des données 2017 au référentiel IRIS

```{r appariement ref IRIS données 2017}

compil_iris_2017 <- filter(redresse_iris_dep_reg, TypeZone=="IRIS", annee==mil, Filiere.de.production!="Cogénération",
                      Filiere.de.production!="Autres filières") %>% # on garde les filières EnR&R et les lignes à l'IRIS
  left_join(select(table_passage_iris_hist, IRIS_INI, IRIS_FIN), by=c("CodeZone"="IRIS_INI")) %>%
  mutate(CodeZone = if_else(is.na(IRIS_FIN), CodeZone, IRIS_FIN)) %>% # mise à jour selon table de passage iris hist INSEE
  left_join(table_passage_com_historique, by=c("com_old"="DEPCOM_HIST")) %>% # ajout du champ code commune à jour
  left_join(select(iris_com_reg, CODE_IRIS, DEPCOM), by=c("DEPCOM")) %>%
  mutate(CodeZone = if_else(is.na(CODE_IRIS), CodeZone, as.character(CODE_IRIS))) %>% # màj codes iris des com fusionnées pr les communes non irisées
  select(-TypeZone, -com_old, -Segment, -annee, -IRIS_FIN, -CODE_IRIS) %>%
  full_join(iris_reg, ., by=c("CODE_IRIS"="CodeZone", "DEPCOM")) %>% # jointure avec le ref des IRIS
  mutate(across(where(is.factor), as.character)) %>% st_drop_geometry() %>% 
  group_by(across(CODE_IRIS:indicateur)) %>% 
  summarise(nb_sites = sum(nb_sites), valeur_redressee = sum(valeur_redressee, na.rm = TRUE), estimation = any(estimation),
                           type_estimation = list(unique(type_estimation)) , .groups = "rowwise") %>%
  mutate(valeur_redressee = if_else(is.na(nb_sites), NA_real_ , valeur_redressee),
         lg_estim =  length(unlist(type_estimation)),
         type_estimation = case_when(
           lg_estim == 1 ~ paste(collapse = ", "),
           lg_estim > 1 ~ setdiff(unlist(type_estimation), "aucune, donnee brute") %>% paste(collapse = ", ")
         )) %>% 
  ungroup %>% 
  select(-lg_estim)
  

iris_inconnus <- filter(compil_iris_2017, is.na(NOM_IRIS), !grepl("xxxx", CODE_IRIS))
iris_factices <- filter(compil_iris_2017, grepl("xxxx", CODE_IRIS))
iris_sans_donnee <- filter(compil_iris_2017, is.na(nb_sites))

data.frame(iris=c("factices", "inconnus", "sans donnees"),
           nombre_iris=c(length(unique(iris_factices$CODE_IRIS)), length(unique(iris_inconnus$CODE_IRIS)), length(unique(iris_sans_donnee$CODE_IRIS))),
           nb_enregistrements=c(nrow(iris_factices), nrow(iris_inconnus), nrow(iris_sans_donnee))
           )

```

En dehors des `r nrow(iris_factices)` enregistrements relatifs à des iris fictifs (cf notice Enedis : _Dans le jeu de données, certains IRIS ont un code INSEE finissant par les caractères XXXX. Il s’agit d’IRIS fictifs qui contiennent les données des sites que l’on n’a pas pu rattacher à leurs IRIS respectifs._), seuls deux IRIS utilisés par Enedis pour les données 2017 ne peuvent pas être appariés à notre référentiel IRIS. Il correspondent à la commune de Trélazé dont le découpage a été entièrement revu en 2019.
A contrario, tous les iris de notre référentiel ne sont appariés à un enregistrement de la table de données fournies par Enedis. Cela correspond à des iris sans installation de production électrique ou non desservis par Enedis. (le tableur initial ne comporte aucune ligne à zéro installation)


```{r sauvegarde donnees Enedis}
save(compil_com_epci_dep_reg_2017, compil_iris_2017, file="collecte/production_elec/Enedis_2017.RData")

```


# calcul des indicateurs
- prod elec renouvelable 2017 par iris / par commune pour carto
- nb d'installations renouvelables par EPCI, DEP, REG 2017
- Evol puissance, nb et prod en base 100, 2008 à 2017, filiere eol / filiere PV; mailles EPCI, DEP et REG
- Puissance eol, puissance PV, par annee, mailles EPCI, DEP, REG



# Datavisualisations

### dataviz indicateurs : Répartition du nb installations par filière en 2017
```{r graph camembert toutes filière MW Nantes métropole, warning=F, message=F}
library(ggplot2)
library(ggthemes)
library(ggiraph)

liste_ter <- filter(communes, EPCI=="244400404", REG==reg) %>%
  select(Epci=EPCI, 'Départements'=DEP, 'Régions'=REG) %>%
  unique() %>%
  gather(key=TypeZone, value=CodeZone)

pie_nm <- inner_join(compil_com_epci_dep_reg_2017, liste_ter) %>%
  filter(grepl("Nombre", indicateur), annee==mil) %>%
  ggplot(aes(CodeZone, valeur, fill=Filiere.de.production)) +
  geom_bar_interactive(aes(tooltip = paste0(Filiere.de.production, " : ", valeur)), stat="identity", position="fill" ) +
  theme_bw(base_size = 12) + scale_fill_economist () +
  labs(title="Répartition du nombre d'installations", x="", y="proportion", caption = paste0("Enedis, ", mil))


```

```{r anim plot 1}
girafe(code = print(pie_nm) ) %>%
girafe_options(opts_tooltip(use_fill = TRUE) )

```



### dataviz indicateurs : Evolution des puissances installées par filière - Graphique


```{r graph évol MW raccordés Nantes Métropole, facette PV, eol }
evol_nm <- inner_join(compil_com_epci_dep_reg_2017, liste_ter)%>%
  filter(grepl("Eolien", Filiere.de.production) | grepl("Photovol", Filiere.de.production),
         grepl("Puissance", indicateur)) %>%
  mutate(annee=as.Date(paste0(annee, "-12-31"))) %>%
  group_by(Filiere.de.production, CodeZone) %>%
  arrange(annee) %>%
  mutate(valeur_indiciee=valeur/valeur[1]*100)%>%
  ungroup %>%
  ggplot() +
  geom_line(aes(x=annee, y=valeur_indiciee, color=CodeZone), size=1 )+
  theme_bw(base_size = 12) + scale_color_economist () +
  labs(title="Evolution des puissances installées (MW)", x="", y="MW", caption = paste0("Enedis, ", mil) ) +
  facet_grid(Filiere.de.production ~., scales="free" )

```

```{r anim plot 2}
# tooltip = paste0(valeur, " MW"))
girafe(code = print(evol_nm) )
# girafe_options(opts_tooltip(use_fill = TRUE))

```


```{r anim plot 2 - plotly}
library(plotly)
ggplotly(evol_nm)

```


### dataviz indicateurs : productions annuelles par filière - Graphique

```{r graph évol MWh produits Nantes Métropole, facette PV, eol }
prod_nm <- filter(compil_com_epci_dep_reg_2017, TypeZone=="Epci", CodeZone=="244400404",
                  grepl("Eolien", Filiere.de.production) | grepl("Photovol", Filiere.de.production),
                  grepl("Energie", indicateur)) %>%
  mutate(annee=as.Date(paste0(annee, "-12-31")), valeur=valeur/1000) %>%
  ggplot(aes(x=annee, y=valeur, tooltip = paste0(prettyNum(valeur, format="d", big.mark=" "), " MWh"))) +
  geom_bar_interactive(stat = "identity", fill="darkslategray4") +
  theme_bw(base_size = 12) +
  labs(title="Productions annuelles (GWh)", x="", y="GWh", caption = paste0("Enedis, ", mil)) +
  facet_grid(Filiere.de.production ~. )
# prettyNum(nb_enr, big.mark = " ")

```

```{r anim plot 3,fig.height=7}
girafe(code = print(prod_nm) )
```

### dataviz indicateurs : Evolution des puissances installées eolien/photovoltaïque - Tableaux

##### Evolution des puissances installées éoliennes
```{r tabl évol MW éoliens raccordés Nantes Métropole, echo=F }
library(kableExtra)
evol_nm_eol <- filter(compil_com_epci_dep_reg_2017, CodeZone=="244400404",
                      grepl("Eolien", Filiere.de.production),
                      grepl("Puissance", indicateur)) %>%
  arrange(annee) %>%
  rename('puissance raccordée au 31/12 (kW)'=valeur) %>%
  mutate(`Année`=substr(annee, 1,4)) %>%
  select(`Année`, everything(), -indicateur, -CodeZone, -TypeZone, -annee)

evol_nm_eol %>% kable(row.names=F, caption = paste0("Enedis, ", mil)) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"))
```

##### Evolution des puissances installées photovoltaïques
```{r tabl évol MW pvq raccordés Nantes Métropole, echo=F }
evol_nm_PV <- filter(compil_com_epci_dep_reg_2017, CodeZone=="244400404",
                      grepl("Photovol", Filiere.de.production),
                      grepl("Puissance", indicateur)) %>%
  arrange(annee) %>%
  rename('puissance raccordée au 31/12 (kW)'=valeur) %>%
  mutate(`Année`=substr(annee, 1,4)) %>%
  select(`Année`, everything(), -indicateur, -CodeZone, -TypeZone, -annee)

evol_nm_PV %>% kable(row.names=F, caption = paste0("Enedis, ", mil)) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"))
```

### Carto communale production EnR&R Nantes métropole


```{r carto Nantes metropole}
nantes_metro <- inner_join(compil_com_epci_dep_reg_2017, filter(liste_zone, EPCI=="244400404")) %>%
  filter(grepl("Energie", indicateur), annee==mil, TypeZone=="Communes") %>%
  select(CodeZone, valeur) %>%
  group_by(CodeZone) %>%
  summarise(valeur=sum(valeur)/1000000) %>%
  inner_join(communes_geo,., by=c("DEPCOM"="CodeZone"))


carte_com <- ggplot(nantes_metro, aes(fill=valeur)) +   geom_sf()+
  labs(title="Production d'électricité renouvelable et de récupération (GWh)",
       caption=paste0("Données Enedis retravaillées par la DREAL - ", mil),
       fill="prod. en GWh") +
  theme(axis.text = element_blank(), panel.background = element_blank(), axis.ticks=element_blank())

```


```{r anim carte com}
carte_com
```

### carte iris prod EnR&R

```{r carto Nantes metropole IRIS}
nantes_metro_iris <- filter(compil_iris_2017, EPCI=="244400404") %>%
  filter(grepl("Energie", indicateur)|is.na(indicateur)) %>%
  select(CODE_IRIS, valeur_redressee) %>%
  group_by(CODE_IRIS) %>%
  summarise(valeur=sum(valeur_redressee, na.rm = T)/1000000, .groups = "drop") %>%
  inner_join(iris_reg %>% select(CODE_IRIS),., by=c("CODE_IRIS"))

carte_iris <- ggplot(nantes_metro_iris, aes(fill=valeur)) + geom_sf() +
  labs(title="Production d'électricité renouvelable et de récupération (GWh)",
       caption=paste0("Données Enedis retravaillées par la DREAL - ", mil),
       fill="prod. en GWh") +
  theme(axis.text = element_blank(), panel.background = element_blank(), axis.ticks=element_blank())

carte_iris

```





