---
title: Sélection et calcul des indicateurs
author: "Juliette Engelaere-Lefebvre"
date: "`r format(Sys.Date(), '%d %B %Y')`"
output:
  html_document:
    df_print: paged
    toc: yes
    toc_depth: '2'
params:
  millesime_elec: 2020
  millesime_gaz: 2020
  region: '52'
---

## Chargement des packages

```{r packages, eval=TRUE, echo=TRUE, warning=FALSE, message=FALSE}
# rm(list=ls())
library(dplyr)
library(sf)
library(tidyr)
library(stringr)
library(purrr)
library(forcats)
library(COGiter)
library(ggplot2)
library(ggiraph)
library(cowplot)
library(ggthemes)
library(plotly)
library(kableExtra)
library(mapview)
library(tibble)
library(data.table)
library(rmarkdown)
library(lubridate)
```

## Modif référentiel EPCI COGiter pour intégrer l'Ile d'Yeu

```{r patch cogiter et ajout ile yeu aux epci et epci geo}

source("collecte/Ajout_communes_insulaires_aux_EPCI.R", encoding = "UTF-8")

```



## Paramètres du script
  
```{r parametres indicateurs, eval=TRUE, echo=TRUE}

reg <- params$region
mil <- params$millesime_elec
mil_gaz <- params$millesime_gaz

```


# Electricité

## Lecture des données collectées et nettoyées

### SDES : consommations dernière année
```{r lecture consommations}
# render('collecte/conso_elec/conso_elec.Rmd', encoding = 'UTF-8', output_format = "html_document",
       # params = list(region=reg, millesime=mil), output_file ="conso_elec.html" , output_dir = "collecte/conso_elec",
       # knit_root_dir=getwd())

conso_elec_iris <- readRDS(paste0("collecte/conso_elec/temp/conso_elec_iris_", mil,"_r",reg,".rds")) %>%
  ungroup() %>% 
  select(-ANNEE) %>%
  mutate(across(where(is.character), as.factor), across(where(is.factor), fct_drop))

conso_elec_com <- readRDS(paste0("collecte/conso_elec/temp/Conso_elec_com_", mil,"_r", reg,".rds")) %>%
  select(-ANNEE) %>%
  mutate(across(where(is.character), as.factor), across(where(is.factor), fct_drop))

conso_elec_epci_a_reg <- readRDS(paste0("collecte/conso_elec/Conso_elec_", mil, "_r",reg,".rds")) %>%
  filter(Echelle != "commune") %>%
  select(-ANNEE)  %>%
  mutate(across(where(is.character), as.factor), across(where(is.factor), fct_drop))
```

### Registre : liste des installations et puissances raccordées par filiere des années 2018 et suivantes

```{r lecture registre}
# render(paste0("collecte/registre/registre_odre_3112", mil, ".Rmd"), encoding = 'UTF-8', 
#        output_format = "html_document",
#        params = list(region=reg, millesime=mil), output_file = paste0("registre_odre_3112", mil,".html"),
#        output_dir = "collecte/registre", knit_root_dir=getwd())

load(paste0("collecte/registre/registre_prod_elec_renouv_r", reg, ".RData")) # installations pour cartes

inst_reg <- inst_reg %>% 
  mutate(REG = if_else(REG == reg, as.character(REG), NA_character_)) %>%
  mutate(across(where(is.factor), as.character), 
         across(where(is.character), enc2utf8),
         typo = as.factor(typo)) %>% 
  # on remet dans l'ordre les colonnes
  select(nominstallation, DEPCOM, IRIS, date_inst, puiss_MW, code_typo, typo, combustible, combustiblessecondaires, technologie, 
         nbgroupes, prod_MWh_an, part_EnR, NOM_DEPCOM, EPCI, NOM_EPCI, DEP, NOM_DEP, REG, NOM_REG, 
         DEPARTEMENTS_DE_L_EPCI, precision_geo, geometry)


typo_registre <- select(as.data.frame(inst_reg), ends_with("typo")) %>%
  unique %>%
  arrange(code_typo) 

load(paste0("collecte/registre/indic_registre_prod_elec_renouv_r", reg, ".RData")) # indicateurs (MW) issus du registre

indic_registre <- filter(indic_registre, millesime == mil, reg_sel) %>%
  select(-millesime, -reg_sel) %>%
  mutate(across(where(is.factor), as.character), 
         across(where(is.character), enc2utf8),
         across(where(is.character), as.factor), 
         across(where(is.factor), fct_drop))
         
```



### Enedis : productions et evolutions des nb installation/puissances raccordées
```{r lecture donnees Enedis}
# render("collecte/production_elec/opendata_Enedis.Rmd", encoding = 'UTF-8', output_format = "html_notebook",
#        params = list(region=reg, millesime=mil), output_file = "opendata_Enedis.html",
#        output_dir = "collecte/production_elec", knit_root_dir=getwd())

load("collecte/production_elec/Enedis.RData")

Enedis_com_a_reg_0 <- compil_com_epci_dep_reg

Enedis_iris <- compil_iris

Enedis_com_a_reg <- Enedis_com_a_reg_0 %>%
  mutate(across(where(is.factor), fct_drop), 
         TypeZone = fct_infreq(TypeZone)) %>%
  arrange(TypeZone, CodeZone, annee) %>%
  mutate(across(where(is.factor), fct_inorder),
         annee = fct_relevel(annee, sort))

# les données à l'iris pour la carto
Enedis_iris_carto <- as.data.frame(compil_iris) %>%
  filter(grepl("Energie", indicateur)|is.na(indicateur),
         !grepl("xxxx", CODE_IRIS), annee == mil) %>%
  select(-indicateur, -estimation, -type_estimation, -annee, -Filiere.de.production, -NOM_IRIS) %>%
  mutate(across(where(is.character), as.factor), across(where(is.factor), fct_drop)) %>%
  group_by(across(where(is.factor))) %>%
  summarise(prod_GWh = sum(valeur, na.rm = T)/1000000, .groups = "drop")


rm(compil_com_epci_dep_reg, compil_iris, Enedis_com_a_reg_0)

```


## Calcul des indicateurs électricité

Une fonction pour catégoriser les proportions EnR&R à utiliser aux différentes échelles
```{r fonction pour catégoriser les prop EnR&R}
cat_part_EnRR <- function(data){
  mutate(data, cat_prct_enrr = cut(pourcent_enrr, breaks = c(0, 10, 25, 50, 100, 200, Inf), 
                                   labels = c("moins de 10%", '10 à 25%', "25 à 50%",
                                              "50% à 100%", "100 à 200%", "plus de 200%")
                                   )
         )
  }


```


### A l'échelle de l'IRIS - carto proportion électricité EnR&R
```{r calcul prop conso renouvelable iris, message=F, comment=F, results='hide', warning=F, prompt=F }
carto_iris_0 <- full_join(conso_elec_iris, Enedis_iris_carto, by = c("id_iris" = "CODE_IRIS")) %>%
  mutate(pourcent_enrr = prod_GWh/CONSO_GWH*100,
         Sources = as.character(map(Sources, ~paste(.x, collapse = ", "))),
         sce_estim = as.character(map(sce_estim, ~paste(.x, collapse = ", ")))) %>%
  cat_part_EnRR() %>%
  mutate(sce_estim = if_else(sce_estim == "donnée brute", "donnée brute", gsub("donnée brute", "", sce_estim)) %>% 
           gsub(", ,", ", ",.) %>% gsub("^, |, $", "",.), 
         across(where(is.factor), as.character),
         across(where(is.character), as.factor), across(where(is.factor), fct_drop)) %>% 
  select(-NOM_DEPCOM, -NOM_EPCI) %>%
  rename(Zone = NOM_IRIS, CODE_IRIS = id_iris)

rm(Enedis_iris_carto)
```


### A l'échelle de la commune - carto proportion électricité EnR&R
```{r calcul prop conso renouvelable com, message=F, comment=F, results='hide', warning=F, prompt=F }
carto_com <- filter(Enedis_com_a_reg, TypeZone == "Communes", grepl("Energie", indicateur)|is.na(indicateur)) %>%
  select(-TypeZone, -annee, -ends_with("timation"), -indicateur) %>%
  group_by(CodeZone) %>%
  summarise(prod_GWh = sum(valeur, na.rm = T)/1000000, .groups = "drop") %>%
  full_join(conso_elec_com, ., by=c("id_com" = "CodeZone")) %>%
  mutate(pourcent_enrr = prod_GWh/CONSO_GWH*100,
        Sources = as.character(map(Sources, ~paste(.x, collapse = ", ")))) %>%
  cat_part_EnRR() %>% 
  inner_join(communes_geo,., by=c("DEPCOM"="id_com"))%>%
  mutate(Sources=paste(Sources, sep=", "), across(where(is.character), as.factor), 
         across(where(is.factor), fct_drop)) %>% 
  rename(Zone=ncom, EPCI=id_epci) %>% 
  select(-nepci, -id_dep, -id_reg, -sce_estim)


```

### Aux échelles epci, departements et région
```{r calcul prop conso renouv epci dep reg, message=F, comment=F, results='hide', warning=F, prompt=F }
carto_epci  <- filter(Enedis_com_a_reg, TypeZone=="Epci", grepl("Energie", indicateur)|is.na(indicateur),
                      annee == mil) %>%
  select(-Filiere.de.production, -annee, -ends_with("timation"), -indicateur) %>%
  group_by(CodeZone, TypeZone) %>%
  summarise(prod_GWh = sum(valeur)/1000000, .groups = "drop") %>%
  inner_join(conso_elec_epci_a_reg, ., by=c("id_ter" = "CodeZone")) %>%
  mutate(pourcent_enrr = prod_GWh/CONSO_GWH*100, 
         Sources = as.character(map(Sources, ~paste(.x, collapse = ", ")))) %>%
  cat_part_EnRR() %>%
  inner_join(epci_geo,., by=c("EPCI" = "id_ter")) %>%
  mutate(EPCI = as.factor(EPCI)) %>%
  rename(Zone = lib_ter) %>% 
  select(-sce_estim)

indic_dep <- filter(Enedis_com_a_reg, TypeZone == "Départements", annee == mil, 
                    grepl("Energie", indicateur)|is.na(indicateur)) %>%
  select(-Filiere.de.production, -annee, -ends_with("timation"), -indicateur) %>%
  group_by(CodeZone, TypeZone) %>%
  summarise(prod_GWh = sum(valeur, na.rm = T)/1000000, .groups = "drop") %>%
  inner_join(conso_elec_epci_a_reg, ., by=c("id_ter" = "CodeZone")) %>%
  mutate(pourcent_enrr = prod_GWh/CONSO_GWH*100,
         Sources = as.character(map(Sources, ~paste(.x, collapse = ", ")))) %>%
  rename(CodeZone = id_ter, Zone = lib_ter)

indic_reg <- filter(Enedis_com_a_reg, TypeZone == "Régions", annee == mil, 
                    grepl("Energie", indicateur)|is.na(indicateur)) %>%
  select(-Filiere.de.production, -annee, -ends_with("timation"), -indicateur) %>%
  group_by(CodeZone, TypeZone) %>%
  summarise(prod_GWh = sum(valeur, na.rm = T)/1000000, .groups = "drop") %>%
  inner_join(conso_elec_epci_a_reg, ., by=c("id_ter" = "CodeZone")) %>%
  mutate(pourcent_enrr = prod_GWh/CONSO_GWH*100,
         Sources = as.character(map(Sources, ~paste(.x, collapse = ", ")))) %>% 
  rename(CodeZone = id_ter, Zone = lib_ter)

indic_epci_a_reg <- select(as.data.frame(carto_epci), TypeZone, Zone, CodeZone = EPCI, pourcent_enrr, Sources,
                           prod_GWh, CONSO_GWH) %>%
  bind_rows(indic_dep, indic_reg) %>%
  mutate(across(where(is.character), as.factor), across(where(is.factor), fct_drop),
         TypeZone = fct_infreq(TypeZone)) %>%
  arrange(TypeZone, CodeZone) %>%
  mutate(across(where(is.factor), fct_inorder))

carto_epci <- select(carto_epci, -TypeZone, -Sources, -prod_GWh, -CONSO_GWH)
rm(indic_dep, indic_reg)

carto_dep <- filter(departements, REG == reg) %>%
  inner_join(departements_geo, .) %>%
  select(DEP, NOM_DEP)

carto_reg <- filter(regions, REG == reg) %>%
  inner_join(regions_geo, .) %>%
  select(REG, NOM_REG)
```

## Péparation des données com et iris pour le fichier à télécharger 'Productions électriques annuelles et capacités raccordées'
On donne à ce fichier la même structure que le fichier d'indicateurs EPCI à REG avec en plus 2 champs pour le filtre de sélection territorial (EPCI et DEP) 

```{r csv prod elec com et iris}
com_iris_csv_elec <- Enedis_iris %>%
  left_join(communes %>% select(DEPCOM, DEP), by = "DEPCOM") %>% 
  select(-TYP_IRIS, -ends_with("DEPCOM"), -NOM_EPCI) %>% 
  rename(CodeZone = CODE_IRIS, Zone = NOM_IRIS) %>% 
  mutate(TypeZone = "Iris") %>% 
  bind_rows(filter(Enedis_com_a_reg, TypeZone == "Communes") %>%
              left_join(select(communes, DEPCOM, Zone = NOM_DEPCOM, EPCI, DEP),
                        by = c("CodeZone" = "DEPCOM"))
            )
  
```



# Création des dataframes utilitaires 
```{r dataframes fonctionnalités appli}
# un DF qui rassemble tous les territoires sélectionnables et comprend l'id ter, la maille et le nom du territoire
liste_zone_complete <- select(indic_epci_a_reg, -pourcent_enrr, -Sources, -sce_estim, -prod_GWh, 
                              -CONSO_GWH, -Echelle) %>%
  mutate(TypeZone = fct_infreq(TypeZone)) %>%
  arrange(TypeZone, CodeZone) %>%
  mutate(across(everything(), fct_inorder))

# une liste des id nommées pour les choix d'inputs
liste_zones = as.list(as.character(liste_zone_complete$CodeZone)) %>%
  setNames(., as.list(as.character(liste_zone_complete$Zone)))

# un ensemble de listes nommées d'EPCI départementales
liste_dep <- filter(departements, REG == reg) %>%
  pull(DEP) %>%
  as.character() %>%
  setNames(filter(departements, REG == reg) %>% pull(NOM_DEP) %>% as.character())

liste_zones_dep = map_dfr(liste_dep, ~filter(liste_zone, grepl(.x, DEP), grepl(reg, REG), TypeZone !="Communes") %>%
                            select(CodeZone:TypeZone) %>% mutate(dep = .x) ) %>%
  left_join(liste_zone_complete) %>%
  mutate_all(as.character) %>%
  select(-TypeZone) %>%
  group_by(dep) %>%
  nest

obj_reg <- filter(Enedis_com_a_reg, CodeZone == reg, annee == 2014, grepl("Eol|Hydr|Photo", Filiere.de.production),
                  grepl("Puis", indicateur)) %>% 
  bind_rows(., ., .) %>%
  mutate(annee=c(rep("2014", 3), rep("2020", 3), rep("2050", 3)), 
         valeur=c(577684, 9163, 348957, 1750000, 14000, 650000, 2300000, 14000, 3000000)) %>%
  mutate(type_estimation = "objectif du SRCAE")
  
# rm(liste_dep) ?



```

# Gaz et biométhane

## Chargement des données
```{r chargement des données biogaz}

# render('collecte/conso_gaz/conso_gaz.Rmd', encoding = 'UTF-8', output_format = "html_document",
#        params = list(region = reg, millesime = mil), output_file ="conso_gaz.html" , output_dir = "collecte/conso_gaz",
#        knit_root_dir=getwd())

# render('collecte/registre_biogaz/registre_biogaz.Rmd', encoding = 'UTF-8', output_format = "html_document",
#        params = list(region=reg, millesime=mil_gaz), output_file ="registre_biogaz.html" ,
#        output_dir = "collecte/registre_biogaz", knit_root_dir=getwd())


load(paste0("collecte/registre_biogaz/indic_biogaz_com_a_reg_", mil_gaz, "_r", reg, ".RData"))
load(paste0("collecte/registre_biogaz/registre_biogaz_", mil_gaz, "_r", reg, ".RData"))

indic_biogaz_epci_a_reg_reg <- mutate(indic_biogaz_epci_a_reg_reg, across(where(is.factor), fct_drop), 
                                      TypeZone = fct_infreq(TypeZone)) %>%
  arrange(TypeZone, CodeZone) %>%
  mutate(across(where(is.factor), fct_inorder))

inst_biogaz_reg <- inst_biogaz_reg %>% 
  # on remet les chaps dans l'ordre
  select(DEPCOM, nom_du_projet, site, grx_demandeur, date_de_mes, annee_mes, capacite_de_production_gwh_an, type_de_reseau, NOM_DEPCOM, 
         EPCI, NOM_EPCI, DEP, NOM_DEP, REG, NOM_REG, DEPARTEMENTS_DE_L_EPCI, REGIONS_DE_L_EPCI, lib_installation,  
         date_mes, code_iris, NOM_IRIS, TYP_IRIS, capacite_dinjection_au_31_12_en_nm3_h, data_annuelles, 
         type, source, geometry) %>% 
  mutate(data_annuelles2 = data_annuelles) %>% 
  unnest(cols  = c(data_annuelles2)) %>%  
  filter(annee == mil) %>% 
  select(-annee) 
  


conso_gaz <- readRDS(paste0("collecte/conso_gaz/conso_gaz_", mil, "_r", reg,".rds")) 

date_registre_biogaz <- metadata$metas$data_processed %>%
  substr(1,10) %>%
  as.Date() %>%
  format("%d/%m/%Y") %>%
  as.character()

```


## Calculs des indicateurs biogaz et assemblage aux indicateurs élec

```{r calculs indic biogaz et jointure indic precedents}

comb_txt <- function(x) {
  b <- unlist(x, recursive = FALSE) %>%
    unique() %>%
    setdiff("")
  if (length(b)==0) return("") else list(b)
}

cat_part_bioch4 <- function(data){
  mutate(data, cat_prct_bioch4 = cut(pourcent_bioch4, breaks = c(-1, 0, 1, 3, 5, 10, Inf), 
                                   labels = c("0%", "moins de 1%", '1 à 3%', "3 à 5%",
                                              "5% à 10%", "plus de 10%")
                                   )
         )
  }

carto_com <- filter(indic_biogaz_com_reg, annee == min(mil_gaz, mil), indicateur == "quantite_annuelle_injectee_en_mwh" ) %>%
  group_by(DEPCOM) %>%
  summarise(source_inject = as.list(comb_txt(source)),
            injections_GWh = sum(valeur)/1000, .groups = "drop") %>% # passage MWh en GWh
  left_join(carto_com, .) %>%
  left_join(filter(conso_gaz, ANNEE == mil_gaz, Echelle == "commune") %>%
               select(DEPCOM = id_ter, conso_gaz_GWh = CONSO_GWH, source_conso_gaz = Sources)) %>%
  replace_na(list(injections_GWh = 0, conso_gaz_GWh = 0, source_inject = "", source_conso_gaz = "")) %>%
  mutate(pourcent_bioch4 = if_else(conso_gaz_GWh == 0, NA_real_, injections_GWh/conso_gaz_GWh*100)) %>%
  cat_part_bioch4() %>%
  select(everything(), -Sources, -starts_with("source"), Sources, starts_with("source"))
  

indic_epci_a_reg <-  filter(indic_biogaz_epci_a_reg_reg, annee == mil_gaz, indicateur == "quantite_annuelle_injectee_en_mwh") %>%
  group_by(CodeZone, Zone, TypeZone) %>%
  summarise(source_inject = as.list(comb_txt(source)), injections_GWh = sum(valeur)/1000, .groups = "drop") %>% 
  full_join(indic_epci_a_reg,  by = c("CodeZone", "TypeZone", "Zone"))%>%
  left_join(filter(conso_gaz, ANNEE == mil_gaz, Echelle != "commune", Echelle != "iris") %>%
               select(CodeZone = id_ter, conso_gaz_GWh = CONSO_GWH, source_conso_gaz = Sources)) %>%
  replace_na(list(injections_GWh = 0, conso_gaz_GWh = 0, source_inject = "", source_conso_gaz = "")) %>%
  mutate(pourcent_bioch4 = if_else(conso_gaz_GWh == 0, 0, injections_GWh/conso_gaz_GWh*100)) %>%
  select(TypeZone, CodeZone, Zone, prod_GWh, CONSO_GWH, pourcent_enrr, everything(), 
         -starts_with("source"), starts_with("source"), -Sources, Sources, -Echelle, -sce_estim) %>%
  ungroup()

carto_epci <- filter(indic_epci_a_reg, TypeZone=="Epci") %>%
  select(EPCI=CodeZone, Zone, pourcent_bioch4) %>%
  cat_part_bioch4() %>%
  inner_join(carto_epci, .)

carto_iris <- filter(indic_biogaz_iris_reg, annee == min(mil_gaz, mil), indicateur == "quantite_annuelle_injectee_en_mwh" ) %>%
  group_by(CODE_IRIS, NOM_IRIS, TYP_IRIS, DEPCOM) %>%
  summarise(source_inject = as.list(comb_txt(source)),
            injections_GWh = sum(valeur)/1000, .groups = "drop") %>% # passage MWh en GWh
  left_join(carto_iris_0 %>% select(-TYP_IRIS, -DEPCOM, -EPCI), ., by = "CODE_IRIS", "Zone" = "NOM_IRIS") %>%
  left_join(filter(conso_gaz, ANNEE == mil_gaz, Echelle == "iris") %>%
               select(CODE_IRIS = id_ter, conso_gaz_GWh = CONSO_GWH, source_conso_gaz = Sources),
            by = "CODE_IRIS") %>%
  replace_na(list(injections_GWh = 0, conso_gaz_GWh = 0, source_inject = "", source_conso_gaz = "")) %>%
  mutate(pourcent_bioch4 = if_else(conso_gaz_GWh == 0, NA_real_, injections_GWh/conso_gaz_GWh*100)) %>%
  cat_part_bioch4() %>%
  select(everything(), -Zone) %>%
  left_join(select(communes, DEPCOM, EPCI, DEP), by = "DEPCOM") %>%
  filter(!grepl("x|X", CODE_IRIS))

```

## Péparation des données com et iris pour le fichier à télécharger 'Injections annuelles de biométhane et capacités d'injection
'
On donne à ce fichier la même structure que le fichier d'indicateurs EPCI à REG avec en plus 2 champs pour le filtre de sélection territorial (EPCI et DEP) 

```{r csv prod biogaz com et iris}

indic_biogaz_iris <- indic_biogaz_iris_reg %>%
  left_join(communes %>% select(DEPCOM, EPCI, DEP), by = "DEPCOM") %>% 
  select(-TYP_IRIS, -DEPCOM) %>% 
  rename(CodeZone = CODE_IRIS, Zone = NOM_IRIS) %>% 
  mutate(TypeZone = "Iris")


com_iris_csv_biogaz <-  indic_biogaz_com_reg %>%
  left_join(select(communes, DEPCOM, Zone = NOM_DEPCOM, EPCI, DEP), by = "DEPCOM") %>% 
  rename(CodeZone = DEPCOM) %>% 
  mutate(TypeZone = "Communes") %>% 
  bind_rows(indic_biogaz_iris)
  
```



# Rassemblement des métadonnées


```{r meta}
metadatas <- data.frame(
  dataset = c(rep("registre électricité", nrow(metaregistre)), paste0("productions électriques à la maille ", metaenedis$maille),
              metadata[["datasetid"]], metadata_grdf[["datasetid"]], metadata_grtgaz[["datasetid"]],
              "Données locales de consommations électriques et gaz"),
  millesime = c(metaregistre$millesime %>% as.Date("%d%m%y") %>% format("%d/%m/%Y") , rep(paste0("2011 à ", mil), nrow(metaenedis)), 
                paste0("en cours ", year(today())), paste0("2013 à ", mil), 
                paste0("2017 à ", mil), paste0("2008 à ", mil)),
  producteur = c(rep("ODRE", nrow(metaregistre)), rep("Enedis", nrow(metaenedis)),
                 metadata[["metas"]][["publisher"]], metadata_grdf[["metas"]][["publisher"]], "GRTgaz",  "SDES"),
  date_publication = c(metaregistre$date_maj, metaenedis$date_maj %>% as.Date() %>% format("%d/%m/%Y"),
                       metadata[["metas"]][["modified"]] %>% substr(1,10) %>% as.Date() %>% format("%d/%m/%Y"),
                       metadata_grdf[["metas"]][["modified"]] %>% substr(1,10) %>% as.Date() %>% format("%d/%m/%Y"), 
                       metadata_grtgaz$metas$data_processed  %>% as.Date %>% format("%d/%m/%Y"),
                       "22/10/2021") 
  )                     

metadatas                     
                     
```

# Sauvegarde du datamart

```{r dataframes chargement appli}
rm(list=setdiff(ls(), c("carto_com", "carto_dep", "carto_epci", "carto_iris", "carto_reg", "date_registre_biogaz", 
                        "Enedis_com_a_reg", "indic_biogaz_epci_a_reg_reg", "indic_epci_a_reg", "indic_registre","lib_filiere",
                        "inst_biogaz_reg", "inst_reg", "liste_zones", "liste_zone_complete", "liste_dep",  "liste_zones_dep",
                        "mil", "mil_gaz", "obj_reg", "reg", "typo_registre", "metadatas", "com_iris_csv_elec",
                        "com_iris_csv_biogaz" )))


save.image(file="app/data_appli.RData")

```

# datavisualisations electricité

## fonctions utilitaires, traitements communs
```{r utils}
is_not_empty <- function(x) all(!is.na(x))
is_pas_zero  <- function(x) {
  if(is.numeric(x)) {all(x!=0)}
  else {TRUE}
}

epci_sel <- "200060010"

liste_ter <- filter(liste_zone_complete, CodeZone %in% c(epci_sel, "49", reg))


```


## onglet photovoltaïque

### 1- infobox MW raccordés au 31/12 (registre)
```{r dataviz PV 1}
filter(indic_registre, CodeZone == epci_sel, grepl("puiss_MW__pvq", variable)) %>%
  summarise(valeur=sum(valeur)) %>% round(1) %>% 
  paste0(. , " MW raccordés au 31 décembre ", mil)
```

### 2- infobox installations en fonctionnement au 31/12 du dernier millesime (Enedis)
```{r dataviz PV 2}
filter(Enedis_com_a_reg, CodeZone==epci_sel, annee==mil, grepl("Nombre", indicateur),
       Filiere.de.production == "Photovoltaïque") %>%
  summarise(valeur=sum(valeur)) %>%
  paste0(. , " installations en fonctionnement en ", mil) %>%
  tricky::french_formatting()

```

### 3- infobox production en MWh du dernier millesime (Enedis)
```{r dataviz PV 3}
filter(Enedis_com_a_reg, CodeZone == epci_sel, annee == mil, grepl("nergie", indicateur),
       Filiere.de.production == "Photovoltaïque") %>%
  summarise(valeur = (valeur/1000000) %>% round(1)) %>%
  paste0(. , " GWh produit en ", mil)
```

### 4- graph1 Courbes d'évolution des MW raccordés en base 100 (Enedis)


```{r dataviz PV 4}
evol_MW_PV <- inner_join(Enedis_com_a_reg, liste_ter, by = c("TypeZone", "CodeZone")) %>%
  filter(grepl("Photovol", Filiere.de.production),
         grepl("Puissance", indicateur)) %>%
  mutate(annee=as.Date(paste0(annee, "-12-31"))) %>%
  group_by(Filiere.de.production, CodeZone) %>%
  arrange(annee) %>%
  mutate(valeur_indiciee=valeur/valeur[1]*100)%>%
  ungroup %>%
  ggplot() +
  geom_line_interactive(aes(x=annee, y=valeur_indiciee, color=Zone, 
                            tooltip=Zone), size=1 ) +
  theme_bw(base_size = 18) + theme(panel.grid = element_blank(), 
                                   panel.background = element_rect(color = "grey90")) +
  scale_color_economist() +
  # labs(title="Evolution des puissances installées \n(indice 100)", x="", y="puissance, indice 100", caption = paste0("Enedis, ", mil) )
  labs(title="", x="", y="", colour = NULL)
  
leg_ter <- get_legend(evol_MW_PV)
ggdraw(leg_ter)

```
```{r}
girafe(ggobj = (evol_MW_PV + guides(colour = "none")), pointsize = 20, width_svg = 6, height_svg = 6) %>%
  girafe_options(opts_tooltip(use_fill = TRUE, opacity = .7))

```


### 5- graph2 Courbes d'évolution du nb d'installations en base 100 (Enedis)
```{r dataviz PV 5}
evol_nb_PV <- inner_join(Enedis_com_a_reg, liste_ter, by = c("TypeZone", "CodeZone")) %>%
  filter(grepl("Photovol", Filiere.de.production),
         grepl("Nombre", indicateur)) %>%
  mutate(annee=as.Date(paste0(annee, "-12-31"))) %>%
  group_by(Filiere.de.production, CodeZone) %>%
  arrange(annee) %>%
  mutate(valeur_indiciee=valeur/valeur[1]*100)%>%
  ungroup %>%
  ggplot() +
  geom_line(aes(x=annee, y=valeur_indiciee, color=Zone), size=1 )+
  theme_bw(base_size = 12) + scale_color_economist () +
  labs(title="Evolution du nombre d'installations (indice 100)", x="", y="nb instal., indice 100", caption = paste0("Enedis, ", mil) )

```
```{r}
ggplotly(evol_nb_PV)
```



### 6- graph3 diagramme barre des productions annuelles en GWh (Enedis)
```{r dataviz PV 6}
bar_prod_pv <- filter(Enedis_com_a_reg, TypeZone == "Epci", CodeZone == epci_sel,
                  grepl("Photovol", Filiere.de.production),
                  grepl("Energie", indicateur)) %>% unique() %>% 
  mutate(annee = as.Date(paste0(annee, "-06-30")), valeur=valeur/1000000) %>%
  ggplot(aes(x=annee, y=valeur, tooltip = paste0(prettyNum(valeur, format="d", big.mark=" "), " GWh"))) +
  geom_bar(stat = "identity", fill="darkslategray4") +
  theme_bw(base_size = 12) +
  labs(title="Productions annuelles (GWh)", x="", y="GWh", caption = paste0("Enedis, ", mil))

```

```{r}
ggplotly(bar_prod_pv)
```

### 7- tableau annuel des puissances raccordées en fin d'année (Enedis)
```{r dataviz PV 7}
inner_join(Enedis_com_a_reg, liste_ter, by = c("TypeZone", "CodeZone")) %>%
  filter(grepl("Photov", Filiere.de.production), grepl("Puissance", indicateur)) %>%
  arrange(annee) %>%
  mutate(`Année`= substr(annee, 1,4), valeur=valeur/1000) %>%
  select(`Année`, everything(), -Filiere.de.production, -indicateur, -TypeZone, -CodeZone,
         -annee, -ends_with("stimation")) %>%
  pivot_wider(everything(), names_from = Zone, values_from = valeur) %>%
  kable(row.names=F, caption = paste0("MW raccordées au 31/12 - Enedis, ", mil)) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"))


```

### 8- vue carto des installations (registre)
```{r dataviz PV 8}
filter(inst_reg, EPCI==epci_sel, code_typo=="pvq") %>%
  st_transform(4326)%>%
  st_jitter(factor = 0.008) %>%
  mapview(zcol="typo", burst=TRUE, legend=TRUE, map.types = c("CartoDB.Positron"), layer.name="type d'installation", label="nominstallation", cex="puiss_MW", alpha = 0)

```

## onglet éolien
### 1- infobox MW raccordés au 31/12
```{r dataviz eolien 1}
filter(indic_registre, CodeZone==epci_sel, grepl("puiss_MW__eol", variable)) %>%
  summarise(valeur=sum(valeur)) %>%
  paste0(. , " MW raccordés au 31 décembre ", mil)
```

### 2- infobox installations en fonctionnement au 31/12 du dernier millesime
```{r dataviz eolien 2}
filter(Enedis_com_a_reg, CodeZone==epci_sel, annee==mil, grepl("Nombre", indicateur),
       Filiere.de.production=="Eolien") %>%
  summarise(valeur = sum(valeur)) %>%
  paste0(. , " installations en fonctionnement en ", mil)

```

### 3- infobox production en MWh du dernier millesime
```{r dataviz eolien 3}
filter(Enedis_com_a_reg, CodeZone==epci_sel, annee==mil, grepl("Energie", indicateur),
       Filiere.de.production=="Eolien") %>%
  summarise(valeur = (valeur/1000000) %>% round(1)) %>%
  paste0(. , " GWh produit en ", mil)

```

### 4- graph1 Courbes d'évolution des MW raccordés en base 100
```{r dataviz eolien 4}
evol_MW_eol <- inner_join(Enedis_com_a_reg, liste_ter, by = c("TypeZone", "CodeZone")) %>%
  filter(grepl("Eol", Filiere.de.production),
         grepl("Puissance", indicateur)) %>%
  mutate(annee=as.Date(paste0(annee, "-12-31"))) %>%
  group_by(Filiere.de.production, CodeZone) %>%
  arrange(annee) %>%
  mutate(valeur_indiciee=valeur/valeur[1]*100)%>%
  ungroup %>%
  ggplot() +
  geom_line(aes(x=annee, y=valeur_indiciee, color=Zone), size=1 )+
  theme_bw(base_size = 12) + scale_color_economist () +
  labs(title="Evolution des puissances installées (indice 100)", x="", y="puissance, indice 100", caption = paste0("Enedis, ", mil) )

```
```{r}
ggplotly(evol_MW_eol)
```

### 5- graph2 Courbes d'évolution du nb d'installations en base 100
```{r dataviz eolien 5}
evol_nb_eol <- inner_join(Enedis_com_a_reg, liste_ter, by = c("TypeZone", "CodeZone")) %>%
  filter(grepl("Eol", Filiere.de.production),
         grepl("Nombre", indicateur)) %>%
  mutate(annee=as.Date(paste0(annee, "-12-31"))) %>%
  group_by(Filiere.de.production, CodeZone) %>%
  arrange(annee) %>%
  mutate(valeur_indiciee=valeur/valeur[1]*100)%>%
  ungroup %>%
  ggplot() +
  geom_line(aes(x=annee, y=valeur_indiciee, color=Zone), size=1 )+
  theme_bw(base_size = 12) + scale_color_economist () +
  labs(title="Evolution du nombre d'installations (indice 100)", x="", y="puissance, indice 100", caption = paste0("Enedis, ", mil) )

```
```{r}
ggplotly(evol_nb_eol)
```

### 6- graph3 diagramme barre des productions annuelles en MWh 
```{r dataviz eolien 6}
bar_prod_eol <- filter(Enedis_com_a_reg, TypeZone=="Epci", CodeZone==epci_sel,
                  grepl("Eol", Filiere.de.production),
                  grepl("Energie", indicateur)) %>% unique() %>% 
  mutate(annee = as.Date(paste0(annee, "-06-30")), valeur=valeur/1000000) %>%
  ggplot(aes(x=annee, y=valeur, tooltip = paste0(prettyNum(valeur, format="d", big.mark=" "), " MWh"))) +
  geom_bar(stat = "identity", fill="darkslategray4") +
  theme_bw(base_size = 12) +
  labs(title="Productions annuelles (GWh)", x="", y="GWh", caption = paste0("Enedis, ", mil))

```

```{r}
ggplotly(bar_prod_eol)
```

### 7- tableau annuel des puissances raccordées en fin d'année (MW)
```{r dataviz eolien 7}
inner_join(Enedis_com_a_reg, liste_ter, by = c("TypeZone", "CodeZone")) %>%
  filter(grepl("Eolien", Filiere.de.production), grepl("Puissance", indicateur)) %>%
  arrange(annee) %>%
  mutate(`Année`= substr(annee, 1,4), valeur=valeur/1000) %>%
  select(`Année`, everything(), -Filiere.de.production, -indicateur, -TypeZone, -annee, 
         -CodeZone, -ends_with("stimation")) %>%
  spread(key = Zone, value=valeur) %>%
  kable(row.names=F, caption = paste0("MW raccordées au 31/12 - Enedis, ", mil)) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"))

```

### 8- vue carto des installations (registre ou SGBD dreal ?)
```{r dataviz eolien 8}
filter(inst_reg, EPCI==epci_sel, code_typo=="eol_ter") %>%
  st_transform(4326)%>%
  st_jitter(factor = 0.008) %>%
  mapview(zcol="typo", burst=TRUE, legend=TRUE, map.types = c("CartoDB.Positron"), layer.name="type d'installation", label="nominstallation", cex="puiss_MW", alpha = 0)

```

## onglet toutes filières electriques Enr&R
### 1- infobox nb installations en fonctionnement au 31/12 du dernier millesime (Enedis)
```{r dataviz ttes filieres 1}

filter(Enedis_com_a_reg, CodeZone==epci_sel, annee==mil, grepl("Nombre", indicateur)) %>%
  summarise(valeur=sum(valeur)) %>%
  paste0(. , " installations en fonctionnement en ", mil)


```

### 2- infobox % EnR&R rapporté à la conso (dernier millésime)
```{r dataviz ttes filieres 2}
filter(indic_epci_a_reg, CodeZone==epci_sel) %>%
  summarise(valeur=round(pourcent_enrr, 1)) %>%
  paste0(. , "% de la consommation électrique couverte par la production EnR&R en ", mil)
```

### 3- graph1 répartition des MW raccordés par filière (dernier millesime)
```{r dataviz ttes filieres 3}
pie_MW <- inner_join(indic_registre, liste_ter, by = c("TypeZone", "CodeZone"))%>%
  filter(grepl("puiss_MW", variable)) %>%
  mutate(code_typo = gsub("puiss_MW__", "", variable)) %>%
  inner_join(typo_registre %>% unique, by = c("code_typo")) %>%
  ggplot(aes(Zone, valeur, fill=typo)) + 
  geom_bar(stat="identity", position="fill" ) + 
  theme_bw(base_size = 12) + scale_fill_economist () + 
  labs(title="Répartition des puissances installées (MW)", x="", y="proportion", caption = paste0("Registre ODRE, ", mil)) 
```

```{r}
ggplotly(pie_MW)
# tooltip = paste0(variable, " : ", valeur)
```


### 4- graph2 répartition des MWh produits par filière (dernier millésime, Enedis)
```{r dataviz ttes filieres 4}
pie_MWh <- inner_join(Enedis_com_a_reg, liste_ter, by = c("TypeZone", "CodeZone"))%>%
  filter(grepl("Energie", indicateur), annee == mil) %>% unique() %>% 
  ggplot(aes(Zone, valeur, fill=Filiere.de.production)) + 
  geom_bar(stat="identity", position="fill" ) + 
  theme_bw(base_size = 12) + scale_fill_economist () + 
  labs(title="Répartition des productions (MWh)", x="", y="proportion", 
       caption = paste0("Enedis, ", mil)) 
```

```{r}
ggplotly(pie_MWh)
# tooltip = paste0(variable, " : ", valeur)
```

### 5- tableau des installations issues du registre (puiss décroissante, paginé)
```{r dataviz ttes filieres 5}
filter(inst_reg, EPCI==epci_sel) %>% as.data.frame() %>%
  mutate(part_EnR=part_EnR*100, date_inst=year(date_inst)) %>%
  select(commune= NOM_DEPCOM, Installation=nominstallation, 'puissance (MW)'=puiss_MW, type=typo,  
          combustible, 'combustible secondaire'=combustiblessecondaires, 'nombre de mâts'=nbgroupes, 
         'production annuelle (MWh)'= prod_MWh_an, 'part renou- velable (%)'=part_EnR, 
         'mise en service'=date_inst, -geometry) %>%
  select_if(is_not_empty) %>% select_if(is_pas_zero)%>%
  arrange(desc(`puissance (MW)`), commune, type, Installation) %>%
  kable(caption = "Installations de production d'électricité renouvelable ou de récupération") %>%
  kable_styling(bootstrap_options = c("striped", "condensed", "responsive"))

```
> a revoir pour choix départemental ou régional (faire un test d'abord sur la maille en input)

### 6- vue carto des installations du registre
```{r dataviz ttes filieres 6}
filter(inst_reg, EPCI==epci_sel)%>%
  st_transform(4326)%>%
  st_jitter(factor = 0.008) %>%
  mapview(zcol="typo", burst=TRUE, legend=TRUE, map.types = c("CartoDB.Positron"), layer.name="type d'installation", label="nominstallation", cex="puiss_MW", alpha = 0)
```
> a revoir pour choix départemental ou régional (faire un test d'abord sur la maille en input)

### 7- carte choroplèthe % elect EnR&R à l'iris
```{r dataviz ttes filieres 7}
filter(carto_iris, EPCI==epci_sel) %>%
  ggplot(aes(fill=pourcent_enrr)) +   geom_sf()+
  labs(title="Part d'électricité renouvelable et de récupération (%)",
       caption=paste0("Données Enedis et SDES retravaillées par la DREAL - ", mil),
       fill="pourcentage") +
  theme(axis.text = element_blank(), panel.background = element_blank(), axis.ticks=element_blank())

```

### 8- carte choroplèthe % elect EnR&R à la commune
```{r dataviz ttes filieres 8}
filter(carto_com, EPCI==epci_sel) %>%
  ggplot(aes(fill=pourcent_enrr)) +   geom_sf()+
  labs(title="Part d'électricité renouvelable et de récupération (%)",
       caption=paste0("Données Enedis et SDES retravaillées par la DREAL - ", mil),
       fill="pourcentage") +
  theme(axis.text = element_blank(), panel.background = element_blank(), axis.ticks=element_blank())
```

### 9- carte choroplèthe % elect EnR&R à l'EPCI (pour choix input département ou région)
```{r dataviz ttes filieres 9}
carto_epci %>%
  ggplot(aes(fill=pourcent_enrr)) +   geom_sf()+
  labs(title="Part d'électricité renouvelable et de récupération (%)",
       caption=paste0("Données Enedis et SDES retravaillées par la DREAL - ", mil),
       fill="pourcentage") +
  theme(axis.text = element_blank(), panel.background = element_blank(), axis.ticks=element_blank())
```





