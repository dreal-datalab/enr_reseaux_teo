library(dplyr)


load("collecte/conso_elec/conso_elec_2008-2019_Frce_entiere.RData")
# conso_elec <- compil_tot %>% 
#   rowwise() %>% 
#   mutate(Sources = paste(as.character(Sources), collapse=", "),
#          sce_estim = paste(as.character(sce_estim), collapse=", ")) %>% 
#   ungroup

conso_elec <- compil_tot %>% 
  rowwise() %>%
  mutate(across(where(is.list), ~ paste0(.x, collapse = ", ", recycle0 = FALSE))) %>% 
  ungroup

write.csv2(x = conso_elec, file = "conso_elec_SDEs_2008-2019_Fce_entiere.csv", row.names = FALSE, fileEncoding = "UTF-8", )



load("collecte/conso_gaz/conso_gaz_2008-2019_Frce_entiere.RData")
conso_gaz <- compil_tot %>% 
  rowwise() %>%
  mutate(across(where(is.list), ~ paste0(.x, collapse = ", ", recycle0 = FALSE))) %>% 
  ungroup

write.csv2(x = conso_gaz, file = "conso_gaz_SDEs_2008-2019_Fce_entiere.csv", row.names = FALSE, fileEncoding = "UTF-8", )
